/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include "arrays.h"


/**
 * ptr_array_uniqueify:
 * @array: an array to uniqueify
 * @compare_func: function to compare two array elements
 *
 * Sort and remove duplicate elements from the given @array, modifying it in
 * place. The given @compare_func takes pointers to pointers in the array,
 * and has the same semantics as strcmp().
 *
 * Since: 0.4.0
 */
void
ptr_array_uniqueify (GPtrArray    *array,
                     GCompareFunc  compare_func)
{
  gsize i;

  /* Sort and uniqueify. */
  g_ptr_array_sort (array, compare_func);

  for (i = 1; i < array->len; i++)
    {
      gpointer element = array->pdata[i];
      gpointer prev_element = array->pdata[i - 1];

      if (compare_func (&element, &prev_element) == 0)
        {
          g_ptr_array_remove_index (array, i - 1);
          i--;
        }
    }
}
