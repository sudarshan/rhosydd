/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "dbus-utilities.h"
#include "librhosydd/proxy-vehicle.h"
#include "librhosydd/subscription.h"
#include "librhosydd/vehicle-interface-generated.c"
#include "librhosydd/vehicle.h"


/* Create a dummy server-side object and a #RsdProxyVehicle for it on the client
 * side, and queue up the given @events to happen after the initial property
 * retrieval. */
static RsdVehicle *
set_up_vehicle (DBusFixture         *fixture,
                const ExpectedEvent *events,
                gsize                n_events,
                guint               *id_out)
{
  g_autoptr (RsdProxyVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (RsdSubscription) subscription = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) unsubscriptions = NULL;
  g_autofree ExpectedEvent *total_events = NULL;
  gsize n_total_events;
  guint id;
  const ExpectedEvent vehicle_events[] = {
    { EVENT_GET_PROPERTY, .get_property = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "VehicleId",
      "'vehicle1'", { 0, 0, NULL }
    }},
    { EVENT_METHOD_CALL, .method_call = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "UpdateSubscriptions",
      "([('*', @a{sv} {})], @a(sa{sv}) [])", "()",
      { 0, 0, NULL }
    }},
  };

  n_total_events = G_N_ELEMENTS (vehicle_events) + n_events;
  total_events = g_new0 (ExpectedEvent, n_total_events);

  memcpy (total_events, vehicle_events, sizeof (vehicle_events));
  memcpy (total_events + G_N_ELEMENTS (vehicle_events), events,
          sizeof (*events) * n_events);

  /* Create the object on the server side. */
  id = dbus_fixture_register_object (fixture, "/",
                                     (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface,
                                     total_events, n_total_events, &error);
  g_assert_no_error (error);

  /* Create the proxy on the client side. */
  data = async_result_data_new ();
  rsd_proxy_vehicle_new_async (fixture->client_connection, NULL, "/", NULL,
                               async_result_data_callback, data);

  async_result_data_yield (data, NULL);

  vehicle = rsd_proxy_vehicle_new_finish (data->result, &error);
  g_assert_no_error (error);
  g_assert (RSD_IS_PROXY_VEHICLE (vehicle));

  /* Subscribe to all attribute notifications. */
  subscription = rsd_subscription_new ("*", NULL, NULL, NULL, 0, G_MAXUINT);
  subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  unsubscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);

  g_ptr_array_add (subscriptions, g_steal_pointer (&subscription));

  async_result_data_free (data);
  data = async_result_data_new ();
  rsd_vehicle_update_subscriptions_async (RSD_VEHICLE (vehicle), subscriptions,
                                          unsubscriptions, NULL,
                                          async_result_data_callback, data);

  async_result_data_yield (data, NULL);

  rsd_vehicle_update_subscriptions_finish (RSD_VEHICLE (vehicle), data->result,
                                           &error);
  g_assert_no_error (error);

  if (id_out != NULL)
    *id_out = id;

  return RSD_VEHICLE (g_steal_pointer (&vehicle));
}

/* Test asynchronous construction of a proxy vehicle. */
static void
test_proxy_vehicle_async_construction (DBusFixture   *fixture,
                                       gconstpointer  test_data)
{
  g_autoptr (RsdProxyVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;
  const ExpectedEvent events[] = {
    { EVENT_GET_PROPERTY, .get_property = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "VehicleId",
      "'vehicle1'", { 0, 0, NULL }
    }},
  };
  g_autoptr (GDBusConnection) connection = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *object_path = NULL;
  g_autoptr (GDBusProxy) proxy = NULL;

  /* Create the object on the server side. */
  dbus_fixture_register_object (fixture, "/",
                                (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface,
                                events, G_N_ELEMENTS (events), &error);
  g_assert_no_error (error);

  /* Create the proxy on the client side. */
  data = async_result_data_new ();
  rsd_proxy_vehicle_new_async (fixture->client_connection, NULL, "/", NULL,
                               async_result_data_callback, data);

  async_result_data_yield (data, NULL);

  vehicle = rsd_proxy_vehicle_new_finish (data->result, &error);
  g_assert_no_error (error);
  g_assert (RSD_IS_PROXY_VEHICLE (vehicle));

  /* Check the vehicle’s attributes. */
  g_assert_cmpstr (rsd_vehicle_get_id (RSD_VEHICLE (vehicle)), ==, "vehicle1");


  /* And its GObject attributes. */
  g_object_get (G_OBJECT (vehicle),
                "connection", &connection,
                "name", &name,
                "object-path", &object_path,
                "proxy", &proxy,
                NULL);

  g_assert_true (G_IS_DBUS_CONNECTION (connection));
  g_assert_cmpstr (name, ==, NULL);
  g_assert_cmpstr (object_path, ==, "/");
  g_assert_true (G_IS_DBUS_PROXY (proxy));
}

/* Test asynchronous construction of a proxy vehicle, which should fail due to
 * receiving invalid attributes from the service. */
static void
test_proxy_vehicle_async_construction_invalid_attributes (DBusFixture   *fixture,
                                                          gconstpointer  test_data)
{
  const struct {
    const gchar *vehicle_id;
    const GError vehicle_id_error;
    RsdVehicleError expected_error_code;
    gboolean expect_type_warning;
  } vectors[] = {
    { "'invalid!'", { 0, 0, NULL },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, FALSE },
    { "55", { 0, 0, NULL },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, TRUE },
    { NULL, { G_DBUS_ERROR, G_DBUS_ERROR_UNKNOWN_PROPERTY, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, FALSE },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdProxyVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      guint id;
      const ExpectedEvent events[] = {
        { EVENT_GET_PROPERTY, .get_property = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "VehicleId",
          vectors[i].vehicle_id, vectors[i].vehicle_id_error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s",
                      i, vectors[i].expected_error_code,
                      vectors[i].vehicle_id);
      /* Do we expect a warning because the property has the wrong type? */
      if (vectors[i].expect_type_warning)
        g_test_expect_message ("GLib-GIO", G_LOG_LEVEL_WARNING,
                               "Received property * with type * does not match "
                               "expected type * in the expected interface");

      /* Create the object on the server side. */
      id = dbus_fixture_register_object (fixture, "/",
                                         (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface,
                                         events, G_N_ELEMENTS (events), &error);
      g_assert_no_error (error);

      /* Create the proxy on the client side. */
      data = async_result_data_new ();
      rsd_proxy_vehicle_new_async (fixture->client_connection, NULL, "/", NULL,
                                   async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      vehicle = rsd_proxy_vehicle_new_finish (data->result, &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (vehicle);

      dbus_fixture_unregister_object (fixture, id);
      g_test_assert_expected_messages ();
    }
}

/* Test asynchronous construction of a proxy vehicle for a vehicle which is
 * missing. */
static void
test_proxy_vehicle_async_construction_missing (DBusFixture   *fixture,
                                               gconstpointer  test_data)
{
  g_autoptr (RsdProxyVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;

  /* Create a proxy on the client side (having not created anything on the
   * server side). */
  data = async_result_data_new ();
  rsd_proxy_vehicle_new_async (fixture->client_connection, NULL, "/", NULL,
                               async_result_data_callback, data);

  async_result_data_yield (data, NULL);

  vehicle = rsd_proxy_vehicle_new_finish (data->result, &error);
  g_assert_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE);
  g_assert_null (vehicle);
}

/* Test construction of a proxy vehicle using an existing proxy. */
static void
test_proxy_vehicle_proxy_construction (DBusFixture   *fixture,
                                       gconstpointer  test_data)
{
  g_autoptr (GDBusProxy) proxy = NULL;
  g_autoptr (RsdProxyVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;
  const ExpectedEvent events[] = {
    { EVENT_GET_PROPERTY, .get_property = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "VehicleId",
      "'vehicle1'", { 0, 0, NULL }
    }},
  };
  g_autoptr (GDBusConnection) connection = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *object_path = NULL;
  g_autoptr (GDBusProxy) proxy2 = NULL;

  /* Create the object on the server side. */
  dbus_fixture_register_object (fixture, "/",
                                (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface,
                                events, G_N_ELEMENTS (events), &error);
  g_assert_no_error (error);

  /* Create a plain GDBusProxy on the client side. */
  data = async_result_data_new ();
  g_dbus_proxy_new (fixture->client_connection, G_DBUS_PROXY_FLAGS_NONE,
                    (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface, NULL,
                    "/", "org.apertis.Rhosydd1.Vehicle", NULL,
                    async_result_data_callback, data);

  async_result_data_yield (data, NULL);

  proxy = g_dbus_proxy_new_finish (data->result, &error);
  g_assert_no_error (error);
  g_assert (G_IS_DBUS_PROXY (proxy));

  /* Create a vehicle to wrap it. */
  vehicle = rsd_proxy_vehicle_new_from_proxy (proxy, &error);
  g_assert_no_error (error);

  /* Check the vehicle’s attributes. */
  g_assert_cmpstr (rsd_vehicle_get_id (RSD_VEHICLE (vehicle)), ==, "vehicle1");


  /* And its GObject attributes. */
  g_object_get (G_OBJECT (vehicle),
                "connection", &connection,
                "name", &name,
                "object-path", &object_path,
                "proxy", &proxy2,
                NULL);

  g_assert_true (G_IS_DBUS_CONNECTION (connection));
  g_assert_cmpstr (name, ==, NULL);
  g_assert_cmpstr (object_path, ==, "/");
  g_assert_true (G_IS_DBUS_PROXY (proxy2));
  g_assert_true (proxy2 == proxy);
}

/* Test construction of a proxy vehicle using an existing proxy, which should
 * fail due to receiving invalid attributes from the service. */
static void
test_proxy_vehicle_proxy_construction_invalid_attributes (DBusFixture   *fixture,
                                                          gconstpointer  test_data)
{
  const struct {
    const gchar *vehicle_id;
    const GError vehicle_id_error;
    RsdVehicleError expected_error_code;
    gboolean expect_type_warning;
  } vectors[] = {
    { "'invalid!'", { 0, 0, NULL },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, FALSE },
    { "55", { 0, 0, NULL },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, TRUE },
    { NULL, { G_DBUS_ERROR, G_DBUS_ERROR_UNKNOWN_PROPERTY, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, FALSE },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GDBusProxy) proxy = NULL;
      g_autoptr (RsdProxyVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      guint id;
      const ExpectedEvent events[] = {
        { EVENT_GET_PROPERTY, .get_property = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "VehicleId",
          vectors[i].vehicle_id, vectors[i].vehicle_id_error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s",
                      i, vectors[i].expected_error_code,
                      vectors[i].vehicle_id);

      /* Do we expect a warning because the property has the wrong type? */
      if (vectors[i].expect_type_warning)
        g_test_expect_message ("GLib-GIO", G_LOG_LEVEL_WARNING,
                               "Received property * with type * does not match "
                               "expected type * in the expected interface");

      /* Create the object on the server side. */
      id = dbus_fixture_register_object (fixture, "/",
                                         (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface,
                                         events, G_N_ELEMENTS (events), &error);
      g_assert_no_error (error);

      /* Create a plain GDBusProxy on the client side. */
      data = async_result_data_new ();
      g_dbus_proxy_new (fixture->client_connection, G_DBUS_PROXY_FLAGS_NONE,
                        (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface, NULL,
                        "/", "org.apertis.Rhosydd1.Vehicle", NULL,
                        async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      proxy = g_dbus_proxy_new_finish (data->result, &error);
      g_assert_no_error (error);
      g_assert (G_IS_DBUS_PROXY (proxy));

      /* Create a vehicle to wrap it. */
      vehicle = rsd_proxy_vehicle_new_from_proxy (proxy, &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (vehicle);

      dbus_fixture_unregister_object (fixture, id);
      g_test_assert_expected_messages ();
    }
}

/* Test getting an attribute from a vehicle. */
static void
test_proxy_vehicle_get_attribute (DBusFixture   *fixture,
                                  gconstpointer  test_data)
{
  g_autoptr (RsdVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) attributes = NULL;
  RsdAttributeInfo *attribute = NULL;
  g_autoptr (GVariant) expected_variant = NULL;
  RsdTimestampMicroseconds current_time;

  const ExpectedEvent events[] = {
    { EVENT_METHOD_CALL, .method_call = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributes",
      "('someAttribute',)", 
      "(@x 150, [('someAttribute',(<'some value'>, 0.0, @x 0),"
      "[{'type', <'attribute'>}, {'unit', <'cm/s'>}],(@u 1, @u 1))])",
      { 0, 0, NULL }
    }}
  };

  vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), NULL);

  /* Get an attribute. */
  data = async_result_data_new ();
  rsd_vehicle_get_attributes_async (RSD_VEHICLE (vehicle), 
                                   "someAttribute",
                                  NULL, async_result_data_callback, data);
  async_result_data_yield (data, NULL);
  attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (vehicle),
                                                data->result, &current_time,
                                                &error);
  attribute = attributes->pdata[0];

  g_assert_no_error (error);

  /* Check the attribute. */
  expected_variant = g_variant_new_string ("some value");

  g_assert_cmpint (current_time, ==, 150);
  g_assert_true (g_variant_equal (attribute->attribute.value, expected_variant));
  g_assert_cmpfloat (attribute->attribute.accuracy, ==, 0.0);
  g_assert_cmpint (attribute->attribute.last_updated, ==, 0);
  g_assert_cmpuint (attribute->metadata.availability, ==,
                    RSD_ATTRIBUTE_AVAILABLE);
  g_assert_cmpuint (attribute->metadata.flags, ==, RSD_ATTRIBUTE_READABLE);
}

/* Test handling an invalid response when getting an attribute from a
 * vehicle. */
static void
test_proxy_vehicle_get_attribute_invalid (DBusFixture   *fixture,
                                          gconstpointer  test_data)
{
  gsize i;
  const struct {
    const gchar *value;  /* in GVariant string format */
    const GError error;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "(@x 10, [('someAttribute',(<'some value'>, -5.0, @x 0),"
       "[{'type', <'attribute'>}, {'unit', <'cm/s'>}],(@u 1, @u 1))])",
       { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 10, [('someAttribute', (<'some value'>, 0.0, @x 0),"
      "[{'type', <'attribute'>}, {'unit', <'cm/s'>}],(@u 100, @u 1))])", 
      { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 10, [('someAttribute', (<'some value'>, 0.0, @x 0),"
      "[{'type', <'attribute'>}, {'unit', <'cm/s'>}],(@u 1, @u 5))])", 
      { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { NULL,
      { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE },
    { NULL,
      { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray) attributes = NULL;
      RsdTimestampMicroseconds current_time = 42;
      guint id;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributes",
          "('someAttribute',)", vectors[i].value, vectors[i].error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s, %s",
                      i, vectors[i].expected_error_code, vectors[i].value,
                      vectors[i].error.message);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get an attribute. */
      data = async_result_data_new ();
      rsd_vehicle_get_attributes_async (RSD_VEHICLE (vehicle),
                                        "someAttribute",
                                        NULL, async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (vehicle),
                                                      data->result, &current_time,
                                                      &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (attributes);
      g_assert_cmpint (current_time, ==, 42);

      dbus_fixture_unregister_object (fixture, id);
    }
}

/* Test getting the metadata for an attribute from a vehicle. */
static void
test_proxy_vehicle_get_metadata (DBusFixture   *fixture,
                                 gconstpointer  test_data)
{
  g_autoptr (RsdVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) metadatas = NULL;
  RsdAttributeMetadata *metadata = NULL;
  RsdTimestampMicroseconds current_time;

  const ExpectedEvent events[] = {
    { EVENT_METHOD_CALL, .method_call = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributesMetadata",
      "('someAttribute',)", "(@x 100, [('someAttribute', [{'type', <'attribute'>}," 
      "{'unit', <'cm/s'>}], (@u 1, @u 1))])",
      { 0, 0, NULL }
    }},
  };

  vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), NULL);

  /* Get an attribute. */
  data = async_result_data_new ();
  rsd_vehicle_get_metadata_async (RSD_VEHICLE (vehicle), 
                                  "someAttribute",
                                  NULL, async_result_data_callback, data);

  async_result_data_yield (data, NULL);

  metadatas = rsd_vehicle_get_metadata_finish (RSD_VEHICLE (vehicle),
                                               data->result, &current_time,
                                               &error);
  metadata = metadatas->pdata[0];
  g_assert_no_error (error);

  /* Check the metadata. */
  g_assert_cmpuint (metadata->availability, ==,
                    RSD_ATTRIBUTE_AVAILABLE);
  g_assert_cmpuint (metadata->flags, ==, RSD_ATTRIBUTE_READABLE);
  g_assert_cmpint (current_time, ==, 100);
}

/* Test handling an invalid response when getting the metadata for an attribute
 * from a vehicle. */
static void
test_proxy_vehicle_get_metadata_invalid (DBusFixture   *fixture,
                                         gconstpointer  test_data)
{
  gsize i;
  const struct {
    const gchar *value;  /* in GVariant string format */
    const GError error;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "(@x 0, [('someAttribute', [{'type', <'attribute'>},"
      "{'unit', <'cm/s'>}], (@u 100, @u 1))])",
      { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1, [('someAttribute', [{'type', <'attribute'>},"
      "{'unit', <'cm/s'>}],(@u 1, @u 5))])",
      { 0, 0, NULL },  RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { NULL,
      { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE },
    { NULL,
      { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },

  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray) metadata = NULL;
      guint id;
      RsdTimestampMicroseconds current_time = 42;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributesMetadata",
          "('someAttribute',)", vectors[i].value, vectors[i].error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s, %s",
                      i, vectors[i].expected_error_code, vectors[i].value,
                      vectors[i].error.message);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get an attribute. */
      data = async_result_data_new ();
      rsd_vehicle_get_metadata_async (RSD_VEHICLE (vehicle), 
                                      "someAttribute",
                                      NULL, async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      metadata = rsd_vehicle_get_metadata_finish (RSD_VEHICLE (vehicle),
                                                  data->result, &current_time,
                                                  &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (metadata);
      g_assert_cmpint (current_time, ==, 42);

      dbus_fixture_unregister_object (fixture, id);
    }
}

/* Test setting the value for an attribute for a vehicle. */
static void
test_proxy_vehicle_set_attribute (DBusFixture   *fixture,
                                  gconstpointer  test_data)
{
  g_autoptr (RsdVehicle) vehicle = NULL;
  g_autoptr (AsyncResultData) data = NULL;
  g_autoptr (GError) error = NULL;
  GHashTable *attribute_values;
  const ExpectedEvent events[] = {
    { EVENT_METHOD_CALL, .method_call = {
      NULL, "/", "org.apertis.Rhosydd1.Vehicle", "SetAttributes",
      "([{'someAttribute', <'hello'>}],)", "()", { 0, 0, NULL }
    }},
  };

  vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), NULL);

  /* Get an attribute. */
  data = async_result_data_new ();

  attribute_values = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                           (GDestroyNotify) g_variant_unref); 
      
  g_hash_table_insert (attribute_values, strdup("someAttribute"),
                       g_variant_ref_sink (g_variant_new_string("hello")));  

  rsd_vehicle_set_attributes_async (RSD_VEHICLE (vehicle),
                                    attribute_values, 
                                    NULL, 
                                    async_result_data_callback,
                                    data);

  async_result_data_yield (data, NULL);

  rsd_vehicle_set_attributes_finish (RSD_VEHICLE (vehicle), data->result, &error);
  g_hash_table_remove_all (attribute_values);
  g_assert_no_error (error);
}

/* Test handling an invalid response when setting the value for an attribute
 * for a vehicle. */
static void
test_proxy_vehicle_set_attribute_invalid (DBusFixture   *fixture,
                                          gconstpointer  test_data)
{
  gsize i;
  const struct {
    const GError error;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE },
    { { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
        (gchar *) "Err" },
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      GHashTable *attribute_values = NULL;
      guint id;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "SetAttributes",
          "([{'someAttribute', <'hello'>}],)", NULL, vectors[i].error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s",
                      i, vectors[i].expected_error_code,
                      vectors[i].error.message);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get an attribute. */
      data = async_result_data_new ();
      attribute_values = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                                (GDestroyNotify) g_variant_unref);
      g_hash_table_insert (attribute_values, strdup("someAttribute"), 
                           g_variant_ref_sink (g_variant_new_string("hello")));  

      rsd_vehicle_set_attributes_async (RSD_VEHICLE (vehicle), 
                                        attribute_values, NULL,
                                        async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      rsd_vehicle_set_attributes_finish (RSD_VEHICLE (vehicle),
                                         data->result, &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);

      g_hash_table_remove_all (attribute_values);
      dbus_fixture_unregister_object (fixture, id);
    }
}

/* Test getting all attributes from a vehicle. */
static void
test_proxy_vehicle_get_all_attributes (DBusFixture   *fixture,
                                       gconstpointer  test_data)
{
  gsize i;
  const struct {
    const gchar *return_value;  /* in GVariant string format */
    gsize n_attributes;
    RsdTimestampMicroseconds current_time;
  } vectors[] = {
    { "(@x 11000, @a(s(vdx)a{sv}(uu)) [])", 0, 11000 },
    { "(@x 150, [( 'someAttribute', (<'some value'>, 0.0, @x 0), [{'type', <'sensor'>}], (@u 1,@u 1))])", 1, 150 },
    { "(@x 0, [( 'someAttribute', (<'some value'>, 0.0, @x 0), [{'unit', <'s'>}],(@u 1,@u 1)),"
        "('otherAttribute', (<'other value'>, 0.0, @x 0), [{'unit', <'cm/s'>}], (@u 1,@u 3))])", 2, 0 },
  /*  { "(@x 10, [( 'someAttribute', (<'some value'>, 0.0, @x 0), [{'availability', <@u 1>}, {'accessibility', <@u 1>}]),"
        "( 'someAttribute', (<'other value'>, 0.0, @x 0), [{'availability', <@u 1>}, {'accessibility', <@u 3>}])])", 2, 10 }, */
    { "(@x 10000000, [( 'unavailableAttribute', (<'some value'>, 0.0, @x 0)," 
         "[{'max', <250>}],(@u 1,@u 1))])", 1, 10000000 } 
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
      RsdTimestampMicroseconds current_time;
      guint id;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributes",
          "('',)", vectors[i].return_value, { 0, 0, NULL }
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting %" G_GSIZE_FORMAT
                      " attributes): %s", i, vectors[i].n_attributes,
                      vectors[i].return_value);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get an attribute. */
      data = async_result_data_new ();
      rsd_vehicle_get_attributes_async (RSD_VEHICLE (vehicle), "", 
                                        NULL,
                                        async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (vehicle),
                                                      data->result,
                                                      &current_time,
                                                      &error);
      g_assert_no_error (error);

      /* Check the attributes. */
      g_assert_cmpint (current_time, ==, vectors[i].current_time);
      g_assert_nonnull (attributes);
      g_assert_cmpuint (attributes->len, ==, vectors[i].n_attributes);

      dbus_fixture_unregister_object (fixture, id);
    }
}

/* Test handling an invalid response when getting all attributes from a
 * vehicle. */
static void
test_proxy_vehicle_get_all_attributes_invalid (DBusFixture   *fixture,
                                               gconstpointer  test_data)
{
  gsize i;
  const struct {
    const gchar *value;  /* in GVariant string format */
    const GError error;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "(@x 1000, [( 'someAttribute', (<'some value'>, -5.0, @x 0),"
      "[{'datatype', <'string'>}], (@u 1, @u 1))])",
      { 0, 0, NULL },
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [( 'someAttribute', (<'some value'>, 0.0, @x 0),"
      "[{'datatype', <'int32'>}],(@u 100, @u 1))])",
      { 0, 0, NULL },
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [( 'someAttribute', (<'some value'>, 0.0, @x 0),@a{sv} [], (@u 1, @u 1))," 
       "( 'otherAttribute', (<'other value'>, 0.0, @x 0),@a{sv} [], (@u 1, @u 100))])",
       { 0, 0, NULL },
       RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [('someAttribute', (<'some value'>, 0.0, @x 0),@a{sv} [], (@u 1, @u 5))])", 
      { 0, 0, NULL },
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [('invalid attribute name!', (<'some value'>, 0.0, @x 0),"
      "[{'type', <'sensor'>}], (@u 1, @u 1))])",
      { 0, 0, NULL },
      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { "(@x 1000, [( 'someAttribute', (<'some value'>, -5.0, @x 0),"
      "[{'type', <'attribute'>}], (@u 1, @u 1)),"
        "('someAttribute', (<'duplicate value'>, -5.0, @x 0),"
        "[{'datatype', <'uint16'>}], (@u 1, @u 1))])",
        { 0, 0, NULL },
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 150, [( 'someAttribute', (<'some value'>, 0.0, @x 151),"
      "[{'maximum-value', <120>}], (@u 1, @u 1))])",
      { 0, 0, NULL },
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { NULL,
      { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE },
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
      RsdTimestampMicroseconds current_time = 42;
      guint id;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributes",
          "('',)", vectors[i].value, vectors[i].error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s, %s",
                      i, vectors[i].expected_error_code, vectors[i].value,
                      vectors[i].error.message);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get an attribute. */
      data = async_result_data_new ();
      rsd_vehicle_get_attributes_async (RSD_VEHICLE (vehicle), "", 
                                        NULL,
                                        async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (vehicle),
                                                      data->result,
                                                      &current_time,
                                                      &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (attributes);
      g_assert_cmpint (current_time, ==, 42);

      dbus_fixture_unregister_object (fixture, id);
    }
}

/* Test getting all metadata from a vehicle. */
static void
test_proxy_vehicle_get_all_metadata (DBusFixture   *fixture,
                                     gconstpointer  test_data)
{
  gsize i;
  const struct {
    const gchar *return_value;  /* in GVariant string format */
    gsize n_metadatas;
    RsdTimestampMicroseconds expected_current_time;
  } vectors[] = {
    { "(@x 100, @a(sa{sv}(uu)) [])", 0, 100 },
    { "(@x 100, [('someAttribute', @a{sv} [], (@u 1, @u 1))])", 1, 100 },
    { "(@x 100, [( 'someAttribute',@a{sv} [],  (@u 1, @u 1)),"
        "( 'otherAttribute',  [{'type', <'attribute'>}], (@u 1, @u 3))])", 2, 100 },
    { "(@x 100, [( 'someAttribute', @a{sv} [], (@u 1, @u 1)),"
        "( 'otherAttribute', [{'unit', <'kmph'>}],(@u 1,@u 3))])", 2, 100 },
    { "(@x 100, [( 'unavailableAttribute', @a{sv} [], (@u 0, @u 0))])", 1, 100 }, 
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
      guint id;
      RsdTimestampMicroseconds current_time;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributesMetadata",
          "('',)", vectors[i].return_value, { 0, 0, NULL }
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting %" G_GSIZE_FORMAT
                      " metadatas): %s", i, vectors[i].n_metadatas,
                      vectors[i].return_value);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get the metadatas. */
      data = async_result_data_new ();
      rsd_vehicle_get_metadata_async (RSD_VEHICLE (vehicle), "", 
                                      NULL, async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      attributes = rsd_vehicle_get_metadata_finish (RSD_VEHICLE (vehicle),
                                                    data->result,
                                                    &current_time, &error);
      g_assert_no_error (error);

      /* Check the metadatas. */
      g_assert_nonnull (attributes);
      g_assert_cmpuint (attributes->len, ==, vectors[i].n_metadatas);
      g_assert_cmpint (current_time, ==, vectors[i].expected_current_time);

      dbus_fixture_unregister_object (fixture, id);
    }
}

/* Test handling an invalid response when getting all metadatas from a
 * vehicle. */
static void
test_proxy_vehicle_get_all_metadata_invalid (DBusFixture   *fixture,
                                             gconstpointer  test_data)
{
  gsize i;
  const struct {
    const gchar *value;  /* in GVariant string format */
    const GError error;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "(@x 1000, [('someAttribute',@a{sv} [], (@u 100, @u 1))])",
      { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [('someAttribute',@a{sv} [], (@u 1, @u 1))," 
        "('otherAttribute',@a{sv} [], (@u 100, @u 1))])",
        { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [('someAttribute', @a{sv} [], (@u 1, @u 5))])",
      { 0, 0, NULL }, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "(@x 1000, [('invalid attribute name!',@a{sv} [], (@u 1, @u 1))])",
      { 0, 0, NULL }, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { "(@x 1000, [('someAttribute',@a{sv} [], (@u 1, @u 1)),"
        "('someAttribute', @a{sv} [], (@u 1, @u 3))])",
        { 0, 0, NULL },  RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { NULL,
      { RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE, (gchar *) "Err" },
      RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE },
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (AsyncResultData) data = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
      guint id;
      RsdTimestampMicroseconds current_time = 42;
      const ExpectedEvent events[] = {
        { EVENT_METHOD_CALL, .method_call = {
          NULL, "/", "org.apertis.Rhosydd1.Vehicle", "GetAttributesMetadata",
          "('',)", vectors[i].value, vectors[i].error
        }},
      };

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expecting error %u): %s, %s",
                      i, vectors[i].expected_error_code, vectors[i].value,
                      vectors[i].error.message);

      vehicle = set_up_vehicle (fixture, events, G_N_ELEMENTS (events), &id);

      /* Get an attribute. */
      data = async_result_data_new ();
      rsd_vehicle_get_metadata_async (RSD_VEHICLE (vehicle), "",
                                      NULL,
                                      async_result_data_callback, data);

      async_result_data_yield (data, NULL);

      attributes = rsd_vehicle_get_metadata_finish (RSD_VEHICLE (vehicle),
                                                    data->result,
                                                    &current_time, &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (attributes);
      g_assert_cmpint (current_time, ==, 42);

      dbus_fixture_unregister_object (fixture, id);
    }
}

static void
invalidated_cb (RsdProxyVehicle *vehicle,
                const GError    *error,
                gpointer         user_data)
{
  GError **error_out = user_data;
  *error_out = g_error_copy (error);
}

/* Test what happens when an #RsdProxyVehicle receives a PropertiesChanged
 * signal from the bus, changing or invalidating the VehicleId or Zones
 * attributes. */
static void
test_proxy_vehicle_change_properties (DBusFixture   *fixture,
                                      gconstpointer  test_data)
{
  const gchar *vectors[] = {
    "('org.apertis.Rhosydd1.Vehicle', {"
       "'VehicleId': <'new-id'>"
    "}, @as [])",
    "('org.apertis.Rhosydd1.Vehicle', @a{sv} {}, ['VehicleId'])",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GVariant) props = NULL;
      guint id;

      g_test_message ("Vector %" G_GSIZE_FORMAT ": %s", i, vectors[i]);

      vehicle = set_up_vehicle (fixture, NULL, 0, &id);

      /* Send the signal. */
      props = g_variant_new_parsed (vectors[i]);
      g_dbus_connection_emit_signal (fixture->server_connection, NULL, "/",
                                     "org.freedesktop.DBus.Properties",
                                     "PropertiesChanged",
                                     g_steal_pointer (&props),
                                     &error);
      g_assert_no_error (error);

      /* Wait for the invalidated signal. */
      g_signal_connect (vehicle, "invalidated", (GCallback) invalidated_cb,
                        &error);
      while (error == NULL)
        g_main_context_iteration (NULL, TRUE);

      g_assert_error (error, RSD_VEHICLE_ERROR,
                      RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR);

      dbus_fixture_unregister_object (fixture, id);
      g_test_assert_expected_messages ();
    }
}

/* Test what happens when an #RsdProxyVehicle receives a AttributesChanged
 * or AttributesMetadataChanged signal from the bus, with valid data. It should
 * be propagated into a #RsdVehicle:attributes-changed or
 * :attributes-metadata-changed signal. */
static void
attributes_changed_cb (RsdVehicle               *vehicle,
                       RsdTimestampMicroseconds  current_time,
                       GPtrArray                *changed_attributes,
                       GPtrArray                *invalidated_attributes,
                       gpointer                  user_data)
{
  gboolean *done = user_data;

  *done = TRUE;
}

static void
attributes_metadata_changed_cb (RsdVehicle               *vehicle,
                                RsdTimestampMicroseconds  current_time,
                                GPtrArray                *changed_attributes_metadata,
                                gpointer                  user_data)
{
  gboolean *done = user_data;

  *done = TRUE;
}

static void
test_proxy_vehicle_signal_valid (DBusFixture   *fixture,
                                 gconstpointer  test_data)
{
  const struct {
    const gchar *signal_name;
    const gchar *expected_signal_name;
    const gchar *signal_parameters;
  } vectors[] = {
    { "AttributesChanged", "attributes-changed",
      "(@x 1000, @a(s(vdx)a{sv}(uu)) ["
        "( 'someAttribute', (<'some value'>, 0.0, @x 0),"
        "@a{sv} [], (@u 1, @u 1))" 
       "],"
       "@a(sa{sv}(uu)) [])" },
    { "AttributesChanged", "attributes-changed",
      "(@x 1000, @a(s(vdx)a{sv}(uu)) [],"
       "@a(sa{sv}(uu)) ["
        "( 'someAttribute', @a{sv} [], (@u 1, @u 1))"
       "])" },
    { "AttributesMetadataChanged", "attributes-metadata-changed",
       "(@x 1000, @a(sa{sv}(uu)) ["
        "( 'someAttribute', @a{sv} [], (@u 1, @u 1))" 
       "])" },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GVariant) props = NULL;
      guint id;
      gboolean received_attributes_changed = FALSE;
      gboolean received_attributes_metadata_changed = FALSE;

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expected signal: %s): %s(%s)",
                      i, vectors[i].expected_signal_name,
                      vectors[i].signal_name,
                      vectors[i].signal_parameters);

      vehicle = set_up_vehicle (fixture, NULL, 0, &id);

      /* Send the signal. */
      props = g_variant_new_parsed (vectors[i].signal_parameters);
      g_dbus_connection_emit_signal (fixture->server_connection, NULL, "/",
                                     "org.apertis.Rhosydd1.Vehicle",
                                     vectors[i].signal_name,
                                     g_steal_pointer (&props),
                                     &error);
      g_assert_no_error (error);

      /* Wait for the propagated signal signal. */
      g_signal_connect (vehicle, "attributes-changed",
                        (GCallback) attributes_changed_cb,
                        &received_attributes_changed);
      g_signal_connect (vehicle, "attributes-metadata-changed",
                        (GCallback) attributes_metadata_changed_cb,
                        &received_attributes_metadata_changed);

      while (!received_attributes_changed &&
             !received_attributes_metadata_changed)
        g_main_context_iteration (NULL, TRUE);

      g_assert (received_attributes_changed !=
                received_attributes_metadata_changed);
      g_assert ((g_strcmp0 (vectors[i].expected_signal_name,
                            "attributes-changed") == 0) ==
                received_attributes_changed);

      dbus_fixture_unregister_object (fixture, id);
      g_test_assert_expected_messages ();
    }
}

/* Test what happens when an #RsdProxyVehicle receives a AttributesChanged
 * or AttributesMetadataChanged signal from the bus, with invalid data. */
static void
test_proxy_vehicle_signal_invalid (DBusFixture   *fixture,
                                   gconstpointer  test_data)
{
  const struct {
    const gchar *signal_name;
    const gchar *signal_parameters;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "AttributesChanged",
      "(@x 1000, @a(s(vdx)a{sv}(uu)) ["
        "( 'not a valid attribute', (<'some value'>, 0.0, @x 0),"
        "@a{sv} [], (@u 1, @u 1))"
       "],"
       "@a(sa{sv}(uu)) [])", RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    /* last_updated > current_time: */
    { "AttributesChanged",
      "(@x 1000, @a(s(vdx)a{sv}(uu)) ["
        "( 'someAttribute', (<'some value'>, 0.0, @x 1001),"
        "@a{sv} [], (@u 1, @u 1))"
       "],"
       "@a(sa{sv}(uu)) [])", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "AttributesChanged",
      "(@x 1000, @a(s(vdx)a{sv}(uu)) [],"
       "@a(sa{sv}(uu)) ["
        "( 'not a valid attribute', @a{sv} [], (@u 1, @u 1))" 
        "])", RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { "AttributesMetadataChanged",
       "(@x 1000, @a(sa{sv}(uu)) ["
        "( 'not a valid attribute', @a{sv} [], (@u 1, @u 1))" 
        "])", RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { "AttributesMetadataChanged",
       "(@x 1000, @a(sa{sv}(uu)) ["
        "( 'someAttribute', @a{sv} [], (@u 100, @u 1))"
        "])", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdVehicle) vehicle = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GVariant) props = NULL;
      guint id;

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expected error: %u): %s(%s)",
                      i, vectors[i].expected_error_code, vectors[i].signal_name,
                      vectors[i].signal_parameters);

      vehicle = set_up_vehicle (fixture, NULL, 0, &id);

      /* Send the signal. */
      props = g_variant_new_parsed (vectors[i].signal_parameters);
      g_dbus_connection_emit_signal (fixture->server_connection, NULL, "/",
                                     "org.apertis.Rhosydd1.Vehicle",
                                     vectors[i].signal_name,
                                     g_steal_pointer (&props),
                                     &error);
      g_assert_no_error (error);

      /* Wait for the invalidated signal. */
      g_signal_connect (vehicle, "invalidated", (GCallback) invalidated_cb,
                        &error);
      while (error == NULL)
        g_main_context_iteration (NULL, TRUE);

      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);

      dbus_fixture_unregister_object (fixture, id);
      g_test_assert_expected_messages ();
    }
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/proxy-vehicle/async-construction", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_async_construction,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/async-construction/invalid-attributes",
              DBusFixture, NULL, dbus_fixture_setup,
              test_proxy_vehicle_async_construction_invalid_attributes,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/async-construction/missing", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_async_construction_missing,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/proxy-construction", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_proxy_construction,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/proxy-construction/invalid-attributes",
              DBusFixture, NULL, dbus_fixture_setup,
              test_proxy_vehicle_proxy_construction_invalid_attributes,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/get-attribute", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_attribute,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/get-attribute/invalid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_attribute_invalid,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/get-metadata", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_metadata,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/get-metadata/invalid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_metadata_invalid,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/set-attribute", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_set_attribute,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/set-attribute/invalid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_set_attribute_invalid,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/get-all-attributes", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_all_attributes,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/get-all-attributes/invalid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_all_attributes_invalid,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/get-all-metadata", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_all_metadata,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/get-all-metadata/invalid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_get_all_metadata_invalid,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/change-properties", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_change_properties,
              dbus_fixture_teardown);

  g_test_add ("/proxy-vehicle/signal/valid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_signal_valid,
              dbus_fixture_teardown);
  g_test_add ("/proxy-vehicle/signal/invalid", DBusFixture, NULL,
              dbus_fixture_setup, test_proxy_vehicle_signal_invalid,
              dbus_fixture_teardown);

  return g_test_run ();
}
