/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <stdarg.h>
#include <string.h>

#include "daemon/aggregate-vehicle.h"
#include "libcroesor/static-vehicle.h"

/* Build a generic #CsrStaticVehicle to test against. It will have the
 * attributes given in the varargs. The varargs must be a %NULL-terminated list
 * of groups of (path, name, value, last_updated) with types (string, string,
 * string, RsdTimestampMicroseconds). */
static CsrStaticVehicle *
build_static_vehicle (const gchar *vehicle_id,
                      ...)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  va_list args;

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

  va_start (args, vehicle_id);

  while (TRUE)
    {
      g_autoptr (RsdAttribute) attribute = NULL;
      g_autoptr (RsdAttributeMetadata) metadata = NULL;
      const gchar  *name, *value;
      RsdTimestampMicroseconds last_updated;
      GVariant *metainfo = NULL;
      GVariantBuilder builder;

      name = va_arg (args, const gchar *);
      if(name == NULL)
         break;      
      value = va_arg (args, const gchar *);
      last_updated = va_arg (args, RsdTimestampMicroseconds);

      attribute = rsd_attribute_new (g_variant_new_string (value), 0.0,
                                     last_updated);

      g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));

      g_variant_builder_add(&builder, "{sv}", "type",
                   g_variant_new_string("attribute"));

      g_variant_builder_add(&builder, "{sv}", "datatype",
                   g_variant_new_string("uint32"));

      metainfo = g_variant_ref(g_variant_builder_end(&builder));

      metadata = rsd_attribute_metadata_new (name, RSD_ATTRIBUTE_AVAILABLE,
                                             RSD_ATTRIBUTE_READABLE, metainfo);

      g_ptr_array_add (attributes,
                       rsd_attribute_info_new (attribute, metadata));
    }

  va_end (args);

  return csr_static_vehicle_new (vehicle_id, attributes);
}

/* Test construction of an aggregate vehicle. */
static void
test_aggregate_vehicle_construction (void)
{
  g_autoptr (VddAggregateVehicle) vehicle = NULL;

  vehicle = vdd_aggregate_vehicle_new ("aggregate0");
}

/* Test that the ID is correct for an aggregate vehicle. */
static void
test_aggregate_vehicle_id (void)
{
  g_autoptr (VddAggregateVehicle) vehicle = NULL;

  vehicle = vdd_aggregate_vehicle_new ("aggregate0");

  g_assert_cmpstr (rsd_vehicle_get_id (RSD_VEHICLE (vehicle)), ==,
                   "aggregate0");
}

static void
get_result_cb (GObject      *obj,
               GAsyncResult *result,
               gpointer      user_data)
{
  GAsyncResult **result_out = user_data;

  *result_out = g_object_ref (result);
}

/* Test successfully retrieving an attribute from an aggregate vehicle with a
 * single source vehicle. */
static void
test_aggregate_vehicle_get_attribute_normal (void)
{
  g_autoptr (CsrStaticVehicle) static_vehicle = NULL;
  g_autoptr (VddAggregateVehicle) aggregate_vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  GPtrArray *attributes = NULL; 
  RsdAttributeInfo *info = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) expected_value = NULL;
  RsdTimestampMicroseconds current_time;
  gint64 local_start_time, local_finish_time;

  static_vehicle = build_static_vehicle ("aggregate0",
                                         "testAttribute", "hello",
                                         RSD_TIMESTAMP_UNKNOWN,
                                         NULL);
  aggregate_vehicle = vdd_aggregate_vehicle_new ("aggregate0");
  vdd_aggregate_vehicle_update_vehicles (aggregate_vehicle,
                                         RSD_VEHICLE (static_vehicle), NULL);

  local_start_time = g_get_monotonic_time ();

  rsd_vehicle_get_attributes_async (RSD_VEHICLE (aggregate_vehicle), 
                                   "testAttribute", NULL, get_result_cb,
                                   &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (aggregate_vehicle),
                                           result, &current_time, &error);
  local_finish_time = g_get_monotonic_time ();

  g_assert_no_error (error);
  g_assert_nonnull (attributes);
  info = attributes->pdata[0]; 
  g_assert_cmpstr (info->metadata.name, ==, "testAttribute");
  expected_value = g_variant_new_string ("hello");
  g_assert (g_variant_equal (info->attribute.value, expected_value));
  g_assert_cmpfloat (info->attribute.accuracy, ==, 0.0);
  g_assert_cmpuint (info->attribute.last_updated, ==, RSD_TIMESTAMP_UNKNOWN);

  /* We know the aggregate vehicle uses the g_get_monotonic_time() clock
   * domain. */
  g_assert_cmpuint (current_time, >=, local_start_time);
  g_assert_cmpuint (current_time, <=, local_finish_time);
}

/* Test retrieving a non-existent attribute fails */
static void
test_aggregate_vehicle_get_attribute_unknown_attribute (void)
{
  g_autoptr (CsrStaticVehicle) static_vehicle = NULL;
  g_autoptr (VddAggregateVehicle) aggregate_vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  GPtrArray *attributes = NULL;
  RsdTimestampMicroseconds current_time = 42;
  g_autoptr (GError) error = NULL;

  static_vehicle = build_static_vehicle ("aggregate0",
                                         "testAttribute", "hello",
                                         RSD_TIMESTAMP_UNKNOWN,
                                         NULL);
  aggregate_vehicle = vdd_aggregate_vehicle_new ("aggregate0");
  vdd_aggregate_vehicle_update_vehicles (aggregate_vehicle,
                                         RSD_VEHICLE (static_vehicle), NULL);


  rsd_vehicle_get_attributes_async (RSD_VEHICLE (aggregate_vehicle), 
                                    "unknownAttribute", NULL, get_result_cb,
                                    &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (aggregate_vehicle),
                                           result, &current_time, &error);

  g_assert_error (error, RSD_VEHICLE_ERROR,
                  RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE);
  g_assert_null (attributes);
  g_assert_cmpuint (current_time, ==, 42);
}

static RsdTimestampMicroseconds
fixed_clock_cb (gpointer user_data)
{
  RsdTimestampMicroseconds clock = GPOINTER_TO_INT (user_data);

  g_debug ("%s: Returning clock value %" G_GINT64_FORMAT ".", G_STRFUNC, clock);

  return clock;
}

/* Test retrieving an attribute from an aggregate vehicle with a single source
 * vehicle correctly adjusts the attribute’s last-updated time from the source
 * vehicle’s clock domain to the aggregate clock domain. */
static void
test_aggregate_vehicle_get_attribute_known_timestamp (void)
{
  g_autoptr (CsrStaticVehicle) static_vehicle = NULL;
  g_autoptr (VddAggregateVehicle) aggregate_vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  GPtrArray *attributes = NULL;
  RsdAttributeInfo *info = NULL;
  g_autoptr (GError) error = NULL;
  gint64 local_start_time, local_finish_time;
  RsdTimestampMicroseconds last_updated_aggregate, current_time_aggregate;
  RsdTimestampMicroseconds last_updated_static, current_time_static;
  gint64 delta;

  last_updated_static = 1000;
  current_time_static = 10000000;

  static_vehicle = build_static_vehicle ("aggregate0",
                                         "testAttribute", "hello",
                                         last_updated_static,
                                         NULL);
  csr_static_vehicle_set_clock_func (static_vehicle, fixed_clock_cb,
                                     GINT_TO_POINTER (current_time_static),
                                     NULL);
  aggregate_vehicle = vdd_aggregate_vehicle_new ("aggregate0");
  vdd_aggregate_vehicle_update_vehicles (aggregate_vehicle,
                                         RSD_VEHICLE (static_vehicle), NULL);

  local_start_time = g_get_monotonic_time ();

  rsd_vehicle_get_attributes_async (RSD_VEHICLE (aggregate_vehicle),
                                    "testAttribute", NULL, get_result_cb,
                                    &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (aggregate_vehicle),
                                            result, &current_time_aggregate,
                                            &error);
  local_finish_time = g_get_monotonic_time ();

  g_assert_no_error (error);
  g_assert_nonnull (attributes);

  info = attributes->pdata[0];
  /* We know the aggregate vehicle uses the g_get_monotonic_time() clock
   * domain. */
  g_assert_cmpuint (current_time_aggregate, >=, local_start_time);
  g_assert_cmpuint (current_time_aggregate, <=, local_finish_time);

  /* The last_updated time on the attribute should be equal to the difference
   * between the static vehicle’s attribute’s last updated time (1000) and the
   * static vehicle’s clock when queried by the aggregate vehicle (10000000),
   * subtracted from the @current_time_aggregate. So we can bound it between
   * adjusted versions of @local_start_time and @local_finish_time. */
  last_updated_aggregate = info->attribute.last_updated;
  delta = current_time_static - last_updated_static;

  g_test_message ("last_updated_static: %" G_GINT64_FORMAT ", "
                  "current_time_static: %" G_GINT64_FORMAT ", "
                  "last_updated_aggregate: %" G_GINT64_FORMAT ", "
                  "current_time_aggregate: %" G_GINT64_FORMAT ", "
                  "local_start_time: %" G_GINT64_FORMAT ", "
                  "local_finish_time: %" G_GINT64_FORMAT ", "
                  "delta: %" G_GINT64_FORMAT,
                  last_updated_static, current_time_static,
                  last_updated_aggregate, current_time_aggregate,
                  local_start_time, local_finish_time, delta);

  g_assert_cmpuint (last_updated_aggregate, >=, local_start_time - delta);
  g_assert_cmpuint (last_updated_aggregate, <=, local_finish_time - delta);
}

/* Test retrieving two attributes from an aggregate vehicle with two source
 * vehicles (with different clock domains) correctly adjusts the attributes’
 * last-updated times so they still have the same relative difference in the
 * aggregate clock domain */
static void
test_aggregate_vehicle_get_attribute_known_timestamp_two_sources (void)
{
  g_autoptr (CsrStaticVehicle) static_vehicle0 = NULL;
  g_autoptr (CsrStaticVehicle) static_vehicle1 = NULL;
  g_autoptr (VddAggregateVehicle) aggregate_vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  GPtrArray /*<RsdAttributeInfo>*/ *attributes = NULL;
  const RsdAttributeInfo *info0, *info1;
  g_autoptr (GError) error = NULL;
  gint64 local_start_time, local_finish_time;
  RsdTimestampMicroseconds current_time_aggregate;
  RsdTimestampMicroseconds last_updated_static0, current_time_static0;
  RsdTimestampMicroseconds last_updated_static1, current_time_static1;
  gint64 delta_static, delta_aggregate;

  last_updated_static0 = 1000;
  current_time_static0 = 10000000;

  last_updated_static1 = 2000;
  current_time_static1 = 20000000;

  static_vehicle0 = build_static_vehicle ("aggregate0",
                                          "testAttribute0", "hello",
                                          last_updated_static0,
                                          NULL);
  csr_static_vehicle_set_clock_func (static_vehicle0, fixed_clock_cb,
                                     GINT_TO_POINTER (current_time_static0),
                                     NULL);

  static_vehicle1 = build_static_vehicle ("aggregate0",
                                          "testAttribute1", "hello",
                                          last_updated_static1,
                                          NULL);
  csr_static_vehicle_set_clock_func (static_vehicle1, fixed_clock_cb,
                                     GINT_TO_POINTER (current_time_static1),
                                     NULL);

  aggregate_vehicle = vdd_aggregate_vehicle_new ("aggregate0");
  vdd_aggregate_vehicle_update_vehicles (aggregate_vehicle,
                                         RSD_VEHICLE (static_vehicle0), NULL);
  vdd_aggregate_vehicle_update_vehicles (aggregate_vehicle,
                                         RSD_VEHICLE (static_vehicle1), NULL);

  local_start_time = g_get_monotonic_time ();

  rsd_vehicle_get_attributes_async (RSD_VEHICLE (aggregate_vehicle), "",
                                    NULL, get_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE (aggregate_vehicle),
                                                  result,
                                                  &current_time_aggregate,
                                                  &error);
  local_finish_time = g_get_monotonic_time ();

  g_assert_no_error (error);
  g_assert_nonnull (attributes);
  g_assert_cmpuint (attributes->len, ==, 2);

  /* Which attribute is which? */
  info0 = attributes->pdata[0];
  info1 = attributes->pdata[1];

  if (g_strcmp0 (info0->metadata.name, "testAttribute1") == 0)
    {
      const RsdAttributeInfo *temp = info1;
      info1 = info0;
      info0 = temp;
    }

  /* We know the aggregate vehicle uses the g_get_monotonic_time() clock
   * domain. */
  g_assert_cmpuint (current_time_aggregate, >=, local_start_time);
  g_assert_cmpuint (current_time_aggregate, <=, local_finish_time);

  /* The difference between the ages of the two attributes should be the same
   * in any clock domain. */
  delta_static = ((current_time_static0 - last_updated_static0) -
                  (current_time_static1 - last_updated_static1));
  delta_aggregate = ((current_time_aggregate - info0->attribute.last_updated) -
                     (current_time_aggregate - info1->attribute.last_updated));

  g_assert_cmpint (delta_static, ==, delta_aggregate);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/aggregate-vehicle/construction",
                   test_aggregate_vehicle_construction);
  g_test_add_func ("/aggregate-vehicle/id", test_aggregate_vehicle_id);
  g_test_add_func ("/aggregate-vehicle/get-attribute/normal",
                   test_aggregate_vehicle_get_attribute_normal);
  g_test_add_func ("/aggregate-vehicle/get-attribute/unknown-attribute",
                   test_aggregate_vehicle_get_attribute_unknown_attribute);
  g_test_add_func ("/aggregate-vehicle/get-attribute/known-timestamp",
                   test_aggregate_vehicle_get_attribute_known_timestamp);
  g_test_add_func ("/aggregate-vehicle/get-attribute/known-timestamp-two-sources",
                   test_aggregate_vehicle_get_attribute_known_timestamp_two_sources);
  return g_test_run ();
}
